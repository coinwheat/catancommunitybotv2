import datetime

from misc_files.constants import (
    game_mode_emoji_mapping,
    game_mode_synonyms,
    match_type_synonyms,
    months,
    periods,
)


class BadFilterError(ValueError):
    pass


async def get_read_filter(words, db):
    date = datetime.date.today()

    period_set = False
    period = "month"
    month = date.month
    year = date.year
    mode = None
    nrof_players = None
    tournament_id = None
    match_type = None

    for word in words:
        if word.lower() in game_mode_synonyms:
            mode = game_mode_synonyms[word.lower()]
        elif word.lower() in months:
            month = months.index(word.lower()) + 1  # 1 based index
            period = None
            if period_set:
                raise BadFilterError(
                    "Cannot specify both a period and a specific month or year"
                )
        elif word.isdigit() and 2000 < int(word) < 10_000:
            year = int(word)
            if period is not None:
                month = None
            if period_set:
                raise BadFilterError(
                    "Cannot specify both a period and a specific month or year"
                )
            period = None
        elif word[-1] == "p" and word[:-1].isdigit():
            nrof_players = int(word[:-1])
            if nrof_players < 0 or nrof_players > 16:
                raise BadFilterError("Number of players must be a positive number < 16")
        elif word in periods:
            if period is None:
                raise BadFilterError(
                    "Cannot specify both a period and a specific month or year"
                )
            period = word
            period_set = True
            if period == "season":
                month = {
                    "$gte": ((date.month - 1) // 3) * 3 + 1,
                    "$lte": ((date.month - 1) // 3) * 3 + 4,
                }
            elif period == "year":
                month = None
            elif period == "month":
                month = date.month
            elif period == "all":
                month = None
                year = None
        elif word.lower() in match_type_synonyms:
            match_type = match_type_synonyms[word.lower()]
        elif word.lower() == "ckleague" and mode is None:
            mode = {"$in": ["c&k", "c&k+seafarers"]}
        elif word.lower() == "baseleague" and mode is None:
            mode = {"$in": ["base", "seafarers"]}
        else:
            tournament = await db["tournaments"].find({"name": word}).to_list(1)
            if len(tournament) == 0:
                raise BadFilterError(f"Unknown rank category: `{word}`")
            else:
                tournament_id = tournament[0]["_id"]
    filter = {
        "month": month,
        "year": year,
        "mode": mode,
        "nrof_players": nrof_players,
        "tournament_id": tournament_id,
        "type": match_type,
    }

    return {key: value for key, value in filter.items() if value is not None}


if __name__ == "__main__":
    test_cases = [
        "month",
        "year",
        "january 2021",
        "c&k 2020",
        "season",
        "c&k season",
        "seafarers february",
        "3p seafarers",
        "dskafjk",
    ]

    for test_case in test_cases:
        print(test_case)
        print(get_read_filter(test_case.split()))
