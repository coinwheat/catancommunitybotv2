import random
import re
import string

colors = [
    "Red",
    "Green",
    "Blue",
    "Orange",
    "Yellow",
    "Black",
    "Pale",
    "Purple",
    "Brown",
    "Azure",
    "Ruby",
    "Chartreuse",
    "Silver",
    "Gold",
    "Copper",
    "Tin",
    "Led",
    "Ice",
    "Iron",
    "Steel",
    "Tungsten",
    "Wooden",
    "Digital",
    "Cardboard",
    "Paper",
    "Alien",
    "Ceramic",
    "Obtuse",
    "Diagonal",
    "Foam",
    "Cold",
    "Lukewarm",
    # Donator Names:
    "Khush",
    "Oreo",
    "Sly",
    "Sami",
    "GB",
    "Mo",
    "full",
    "Aracknid",
    "BuiltDis",
    "vidb0ard",
    "Lynx",
    "TBY",
    "Don",
    "Dy",
    "Under",
    "Saint",
    "Drama",
    "Crimson",
    "Wheat",
    "Karm",
    "Dandy",
    "Albino",
    "Vamos",
    "Cracka",
    "fRDn",
    "Brian",
    # Others
    "Nordic",
    "Re",
    "Activated",
    "X",
    "Ray",
    "Space",
    "Trash",
    "Abbygail",
    "East",
    "Mouse",
]

items = [
    "Road",
    "Village",
    "City",
    "Point",
    "Tile",
    "Wheat",
    "Brick",
    "Wool",
    "Lumber",
    "Ore",
    "Robber",
    "Knight",
    "Monopoly",
    "Army",
    "Gold",
    "Trophy",
    "Pirate",
    "Ship",
    "Hex",
    "Empire",
    "Dice",
    "Number",
    "Meeple",
    "Sentience",
    "WorldDomination",
    "PaperclipDilemma",
    "Sheep",
    "João",
    # Donator Names
    "Wookie",
    "George",
    "Grigs",
    "Gannes",
    "Hunter",
    "Stronaut",
    "Lantern",
    "Two2",
    "Nelson" "ban282",
    "Gamer" "NL",
    "Jas",
    "Jazz",
    "Athanais",
    "Fun",
    "Pepegasus",
    "Planks",
    "Lighted",
    "InBoxes",
    "Cover",
    "LLama",
    "Yelfs",
    "Lotus",
    "OGJE",
    "TK",
    "Coin",
    "Marising",
    "Ritter",
    "BlackHawk",
    "Dark",
    "Arnoud",
    "Rang",
    "Dreams",
    "Hunter",
    "Teenah",
    "Packin",
    "Anora",
    "Tools",
    "KellyC"
    # Others
    "Zombie",
    "Markable",
    "Puzzles",
    "Tony",
    "DiscordBot(beta)",
    "Loaf",
    "Stardust",
    "Hippo",
    "Farmer",
    "Man",
    "Parrot",
    "Zebra",
    "Space",
    "Tail",
    "Santos",
    "Sachin",
    "NikkiB",
    "Giggs",
    "MentalMold",
    "Bathroomfun",
    "DandyDrew",
    "Rigatoni",
    "Aracknid",
    "dtools22",
    "harryshower",
    "mc2HUN",
    "Artful",
    "Peepeepoopoo",
    "Arod",
    "MilenaS",
    "Trissie",
    "MrPlanks",
    "JoãoDark",
    "Marius",
    "JoshuaWright",
    "Saranater",
    "Patrik",
    "WheatCoin",
]


def generate_id():
    return (
        random.choice(colors)
        + random.choice(items)
        + str(random.randint(1, 999))
        + random.choice(string.ascii_uppercase)
    )


expr = re.compile(
    "^("
    + "|".join(re.escape(i) for i in colors)
    + ")("
    + "|".join(re.escape(i) for i in items)
    + r")\d{1,3}[A-Z]$"
)


def match(msg):
    return expr.match(msg)


if __name__ == "__main__":
    print(expr)
    print(len(colors), len(items))
    print(len(colors) * len(items) * 1000 * len(string.ascii_uppercase))
    print((len(colors) * len(items) * 1000 * len(string.ascii_lowercase)) ** 0.5)
