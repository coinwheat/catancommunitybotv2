import datetime
import random
import re
import traceback
import typing
from collections import namedtuple
from typing import Optional

from misc_files.constants import (
    FEMALE_ROLE_ID,
    MALE_ROLE_ID,
    SETTINGS,
    game_mode_emoji_mapping,
    match_info_template,
)
from .generate_streak_image import generate_streak_image


import aiofiles as aiofiles
import asyncio
from dataclasses import dataclass, field

import bson
import discord
import pymongo
import pymongo.errors

from . import total_size, id_generator

# from commands import invites


@dataclass
class MatchSettings:
    type: str
    mode: str
    map: str
    nrof_players: int
    year: int = field(default_factory=lambda: datetime.date.today().year)
    month: int = field(default_factory=lambda: datetime.date.today().month)
    tournament_name: Optional[str] = None
    tournament_id: Optional[bson.ObjectId] = None
    game_id: Optional[bson.objectid.ObjectId] = None


@dataclass
class MatchResults:
    lp: int
    matches: int
    normalized_wins: int
    reason: Optional[str]


async def aenumerate(gen, start=0):
    async for elem in gen:
        yield start, elem
        start += 1


class BotState:
    def __init__(self, db):
        self.db = db
        self.adminroles = []
        self.message_ids = {}
        self.link = ""
        self.tasks = []
        self.role_mention_times = {}

        self.settings = {key: i.default for key, i in SETTINGS.items()}

        self.confirm_game_lock = asyncio.Lock()
        self.cancel_game_lock = asyncio.Lock()
        self.daily_bonus_lock = asyncio.Lock()

        # self.invite_manager = invites.InviteManager()

    async def init(self, client):
        roles = await self.db["admin_roles"].find().to_list(500)
        self.adminroles = [i["role_id"] for i in roles]

        link = await self.db["links"].find_one({"type": "rank"})
        if link is not None:
            self.settings = {**self.settings, **link}

        # await self.invite_manager.update_invites(client.guilds[0])

    @staticmethod
    def format_game_message(game_id, wager, link, players, mode):
        if wager == 0:
            wager_text = 0
        else:
            wager_text = f"⚠️ {wager} ⚠️"

        mode = (
            game_mode_emoji_mapping.get(mode, "")
            + mode
            + game_mode_emoji_mapping.get(mode, "")
        )
        return match_info_template.format(
            id=game_id,
            players="\n".join("> " + i for i in players),
            wager=wager_text,
            link=link,
            mode=mode,
        )

    @staticmethod
    def normalize_mention(mention):
        assert mention.startswith("<@") and mention.endswith(
            ">"
        ), f"Invalid mention: {mention}"
        if mention[2] == "!":
            mention = "<@" + mention[3:]
        return mention

    @staticmethod
    def get_pronoun(user):
        if isinstance(user, discord.Member):
            for role in user.roles:
                if role.id == MALE_ROLE_ID:
                    return "he", "him"
                elif role.id == FEMALE_ROLE_ID:
                    return "she", "her"
        return "they", "them"

    def get_total_size(self):
        admin_roles_size = total_size.total_size(self.adminroles)
        message_ids_size = total_size.total_size(self.message_ids)
        settings_size = total_size.total_size(self.settings)
        role_size = total_size.total_size(self.role_mention_times)
        print(
            f"Sizes: {admin_roles_size=} {message_ids_size=} {settings_size=} {role_size=}"
        )

        return (
            admin_roles_size
            + message_ids_size
            + len(self.link)
            + settings_size
            + role_size
        )

    def is_admin(self, user: discord.Member):
        if user.id in (
            278111514123304961,  # I should probably know who these people are in case somebody goes rogue again
            410606606183825428,
            562731660546801674,
            703031322377060402,
            480772343464067092,
        ):  # I'm always admin myself
            return True
        for role in user.roles:
            if role.id in self.adminroles:
                return True
        if user.guild_permissions.is_superset(discord.Permissions.all()):
            return True
        return False

    def is_donator(self, user: discord.Member):
        for role in user.roles:
            if role.id == self.settings.get("donator_role"):
                return True
        return False

    async def fix_invalid_game_ids(self):
        await self.db["matches"].update_many(
            {}, [{"$set": {"id": {"$toString": "$_id"}}}]
        )

    @staticmethod
    def split_words(message):
        return re.split(r"\s+", message)

    async def create_game(
        self, author, wager=0, link="", game_mode="base", game_display_id=""
    ):
        await self.db["matches"].insert_one(
            {
                "creator": self.normalize_mention(author.mention),
                "players": [self.normalize_mention(author.mention)],
                "approved_players": [],
                "winner": None,
                "total_lp_wagered": 25 + wager,
                "wager": wager,
                "approved": False,
                "mod_approved": False,
                "link": link,
                "id": game_display_id,
                "mode": game_mode,
            }
        )

        await self.update_player_info(
            self.normalize_mention(author.mention),
            last_game_id=game_display_id,
            discord_user=author,
        )
        return game_display_id

    def is_valid_game_id(self, game_id):
        return id_generator.match(game_id) or (
            len(game_id) == 24 and re.fullmatch("^[0-9a-f]{24}$", game_id)
        )

        # return len(game_id) == 24 and re.fullmatch('^[0-9a-f]{24}$', game_id)

    async def get_game_by_id(self, game_id):
        return await self.db["matches"].find_one({"id": game_id})

    async def get_user_by_name(self, name, sort=None):
        if not sort:
            return await self.db["users"].find_one({"discord_username": name})
        else:
            users = (
                await self.db["users"]
                .aggregate(
                    [
                        {"$match": {"discord_username": name}},
                        {"$set": {"sort_lp": sort}},
                    ]
                )
                .to_list(1)
            )
            if len(users) == 0:
                raise ValueError("Cannot find a user with that id: " + name)
            return users[0]

    def get_user_display_name(self, user: discord.Member):
        if user is None:
            return "[Pending Assignment]"
        return (
            user.name
            + "#"
            + str(user.discriminator)
            + ("" if user.nick is None else ' "' + user.nick + '"')
        )

    async def get_extra_player_info(self, discord_user: discord.Member):
        if discord_user is None:
            return {"display_name": "[pending assignment]"}

        return {
            "display_name": self.get_user_display_name(discord_user),
            "is_admin": self.is_admin(discord_user),
            "is_donator": self.is_donator(discord_user),
            "profile_picture_url": str(discord_user.avatar),
        }

    async def update_player_info(
        self,
        player_id: str,
        discord_user: typing.Optional[discord.Member] = None,
        last_game_id=None,
    ):
        user = await self.db["users"].find_one({"discord_username": player_id})
        if not user:
            print(
                f"[USRMNGMT] Making new user: {discord_user and discord_user.display_name} ({player_id}"
            )
            inserted_id = (
                await self.db["users"].insert_one(
                    {
                        "discord_username": player_id,
                        "colonist_username": None,
                        **await self.get_extra_player_info(discord_user),
                        "last_game_id": last_game_id,
                    }
                )
            ).inserted_id
        else:
            if last_game_id:
                await self.db["users"].update_one(
                    {
                        "_id": user["_id"],
                    },
                    {
                        "$set": {
                            "last_game_id": last_game_id,
                            **(
                                await self.get_extra_player_info(discord_user)
                                if discord_user
                                else {}
                            ),
                        }
                    },
                )

            inserted_id = user["_id"]
        return inserted_id

    async def add_lp_to_player(
        self,
        player_id,
        match_settings: MatchSettings,
        match_results: MatchResults,
        discord_user=None,
    ):
        inserted_id = await self.update_player_info(
            player_id, discord_user=discord_user, last_game_id=match_settings.game_id
        )

        if match_results.lp != 0:
            await self.db["lp_events"].insert_one(
                {
                    "player": player_id,
                    "lp": match_results.lp,
                    "reason": match_results.reason,
                    "time": datetime.datetime.now(),
                }
            )

        await self.db["user_lp"].update_one(
            {
                "user": inserted_id,
                "mode": match_settings.mode,
                "map": match_settings.map,
                "type": match_settings.type,
                "nrof_players": match_settings.nrof_players,
                **(
                    {
                        "tournament_name": match_settings.tournament_name,
                        "tournament_id": match_settings.tournament_id,
                    }
                    if match_settings.tournament_name
                    else {}
                ),
                "year": match_settings.year,
                "month": match_settings.month,
            },
            {
                "$inc": {
                    "lp": match_results.lp,
                    "matches": match_results.matches,
                    "normalized_wins": match_results.normalized_wins,
                }
            },
            upsert=True,
        )

    async def get_leaderboard(self, nr=25, lp_filter=None):
        if lp_filter is None:
            lp_filter = {}
        users = self.db["user_lp"].aggregate(
            [
                {"$match": lp_filter},
                {
                    "$group": {
                        "_id": {"user": "$user"},
                        "lp": {"$sum": "$lp"},
                        "matches": {"$sum": "$matches"},
                        "normalized_wins": {"$sum": "$normalized_wins"},
                    }
                },
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "_id.user",
                        "foreignField": "_id",
                        "as": "user_data",
                    }
                },
                {"$sort": {"lp": -1, "user_data.display_name": 1, "matches": -1}},
                {"$limit": nr},
            ]
        )
        last_lp = 99999999
        last_rank = 0

        async for index, user in aenumerate(users):
            if last_lp == user.get("lp", 0):
                yield last_rank, user
            else:
                last_lp = user.get("lp", 0)
                last_rank = index + 1
                yield last_rank, user
                if last_rank + 1 >= nr:
                    break

    async def get_users_ranked_above(self, lp, lp_filter):
        try:
            return (
                await self.db["user_lp"]
                .aggregate(
                    [
                        {"$match": lp_filter},
                        {
                            "$group": {
                                "_id": {"user": "$user"},
                                "lp": {"$sum": "$lp"},
                            }
                        },
                        {"$match": {"lp": {"$gt": lp}}},
                        {"$count": "count"},
                    ]
                )
                .to_list(1)
            )[0]["count"]
        except IndexError:
            return 0

    async def get_next_ranked_user(self, lp, lp_filter):
        cursor = (
            await self.db["user_lp"]
            .aggregate(
                [
                    {"$match": lp_filter},
                    {
                        "$group": {
                            "_id": {"user": "$user"},
                            "lp": {"$sum": "$lp"},
                        }
                    },
                    {"$match": {"lp": {"$gt": lp}}},
                    {"$sort": {"lp": 1}},
                    {"$limit": 1},
                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "_id.user",
                            "foreignField": "_id",
                            "as": "user_data",
                        }
                    },
                ]
            )
            .to_list(1)
        )
        return cursor[0] if cursor else None

    async def remove_user(self, match, user):
        await self.db["matches"].update_one(
            {"_id": match["_id"]},
            {
                "$pull": {"players": self.normalize_mention(user)},
                "$inc": {"total_lp_wagered": -25 - match["wager"]},
            },
        )
        players = (await self.db["matches"].find_one({"_id": match["_id"]}))["players"]
        return players

    @staticmethod
    def extract_nick(player):
        parts = player.split('"')
        if len(parts) != 3:
            return parts[0], parts[0]
        name = '"'.join(parts[:-2])
        nick = parts[-2]
        return name.strip(), nick

    async def daily_bonus(
        self, game_mode, player_id, win=False, send_message_function=None
    ):
        task = asyncio.create_task(
            self._daily_bonus(game_mode, player_id, win, send_message_function)
        )
        self.tasks.append(task)

        for task in self.tasks:
            if task.done():
                task.result()
        self.tasks = [i for i in self.tasks if not i.done()]

    async def _daily_bonus(
        self, game_mode, player_id, win=False, send_message_function=None
    ):
        try:
            async with self.daily_bonus_lock:
                player = await self.get_player_by_id(
                    player_id,
                    {
                        "display_name": True,
                        "last_bonus_game_time": True,
                        "last_bonus_win_time": True,
                        "bonus_calender": True,
                    },
                )
                now = datetime.datetime.utcnow()

                if player:
                    last_bonus_game_time = (
                        player.get("last_bonus_game_time", {})
                        .get(game_mode, datetime.datetime.fromtimestamp(0))
                        .date()
                    )
                    last_bonus_win_time = (
                        player.get("last_bonus_win_time", {})
                        .get(game_mode, datetime.datetime.fromtimestamp(0))
                        .date()
                    )
                else:
                    last_bonus_game_time = datetime.datetime(year=2011, month=1, day=1)
                    last_bonus_win_time = datetime.datetime(year=2011, month=1, day=1)

                got_game_bonus = False
                got_win_bonus = False
                if self.settings.get("first_game_bonus", 0) and (
                    not player or last_bonus_game_time != now.date()
                ):
                    await self.add_lp_to_player(
                        self.normalize_mention(player_id),
                        MatchSettings(
                            mode=game_mode,
                            nrof_players=0,
                            map="base",
                            type="mm",
                        ),
                        MatchResults(
                            lp=self.settings.get("first_game_bonus", 0),
                            matches=0,
                            normalized_wins=0,
                            reason="Daily bonus for first "
                            + game_mode
                            + " game of the day",
                        ),
                    )

                    got_game_bonus = True
                if (
                    self.settings.get("first_win_bonus", 0)
                    and win
                    and (not player or last_bonus_win_time != now.date())
                ):
                    await self.add_lp_to_player(
                        self.normalize_mention(player_id),
                        MatchSettings(
                            mode=game_mode,
                            nrof_players=0,
                            map="base",
                            type="mm",
                        ),
                        MatchResults(
                            lp=self.settings.get("first_win_bonus", 0),
                            matches=0,
                            normalized_wins=0,
                            reason="Daily bonus for first "
                            + game_mode
                            + " win of the day",
                        ),
                    )

                    got_win_bonus = True
                print(f"[BONUS] Giving daily bonus: {got_win_bonus=} {got_game_bonus=}")
                update = {}
                if got_game_bonus or got_win_bonus:
                    update = {"$set": {f"last_bonus_game_time.{game_mode}": now}}

                if got_win_bonus and win:
                    update["$set"][f"last_bonus_win_time.{game_mode}"] = now

                if got_game_bonus or got_win_bonus:
                    if now.isocalendar()[1] != last_bonus_game_time.isocalendar()[1]:
                        update["$set"][f"bonus_calender.{game_mode}"] = [None] * 7
                    else:
                        update["$set"][f"bonus_calender.{game_mode}"] = player.get(
                            "bonus_calender", {}
                        ).get(game_mode, [None] * 7)
                    update["$set"][f"bonus_calender.{game_mode}"][
                        now.isoweekday() - 1
                    ] = ("full" if got_win_bonus else "half")

                    image = await generate_streak_image(
                        update["$set"][f"bonus_calender.{game_mode}"],
                        game_mode,
                        day_bonus=self.settings.get("first_game_bonus", 0),
                        win_bonus=self.settings.get("first_win_bonus", 0),
                    )

                if got_game_bonus and got_win_bonus:
                    await send_message_function(
                        f"Congratulations, {player_id}, you have earned a"
                        f" {self.settings.get('first_win_bonus', 0) + self.settings.get('first_win_bonus', 0)} LP bonus for"
                        f" your first game and first win today in"
                        f" {game_mode_emoji_mapping.get(game_mode, '')} {game_mode}",
                        file=discord.File(image, filename="rank.png"),
                    )
                elif got_win_bonus:
                    await send_message_function(
                        f"Congratulations, {player_id}, you have earned a"
                        f" {self.settings.get('first_win_bonus', 0)} LP bonus for your first win today in"
                        f" {game_mode_emoji_mapping.get(game_mode, '')} {game_mode}",
                        file=discord.File(image, filename="rank.png"),
                    )
                elif got_game_bonus:
                    await send_message_function(
                        f"Congratulations, {player_id}, you have earned a"
                        f" {self.settings.get('first_game_bonus', 0)} LP bonus for your first game "
                        f"today in {game_mode_emoji_mapping.get(game_mode, '')} {game_mode}",
                        file=discord.File(image, filename="rank.png"),
                    )

                if got_game_bonus or got_win_bonus or update and update["$set"]:
                    await self.db["users"].update_one(
                        {
                            "_id": player["_id"],
                        },
                        update,
                    )
        except Exception:
            traceback.print_exc()

    async def win_match(
        self,
        match,
        winner,
        winner_discord_user: typing.Optional[discord.Member] = None,
        admin_approved=False,
        message="Won match {}",
        send_message_function=None,
    ):
        async with self.confirm_game_lock:
            match_settings = MatchSettings(
                type="mm",
                mode=match.get("mode", "base"),
                map="base",
                nrof_players=len(match["players"]),
            )

            if len(match["players"]) == 2:
                wager = min(
                    self.settings.get("max_wager_2p", float("inf")), match["wager"]
                )
            else:
                wager = match["wager"]
            n = len(match["players"])
            total_lp_wagered = (wager * n) + 5 * n * (n + 1)

            await self.db["matches"].update_one(
                {"_id": match["_id"]},
                {
                    "$set": {
                        "winner": winner,
                        "approved": True,
                        "admin_approved": admin_approved,
                        "winner_id": winner_discord_user and winner_discord_user.id,
                        "admin_cancelled": False,
                        "total_lp_wagered": total_lp_wagered,
                    }
                },
            )

            await self.add_lp_to_player(
                winner,
                discord_user=winner_discord_user,
                match_settings=match_settings,
                match_results=MatchResults(
                    total_lp_wagered - wager,
                    matches=1,
                    reason="Won " + message,
                    normalized_wins=len(match["players"]),
                ),
            )
            if len(match["players"]) > 2:
                await self.daily_bonus(
                    match_settings.mode, winner, True, send_message_function
                )

            for player in match["players"]:
                if player != winner:
                    await self.add_lp_to_player(
                        player,
                        match_settings=match_settings,
                        match_results=MatchResults(
                            lp=int(-wager),
                            matches=1,
                            normalized_wins=0,
                            reason="Lost " + message,
                        ),
                    )
                    if len(match["players"]) > 2:
                        await self.daily_bonus(
                            match_settings.mode, player, False, send_message_function
                        )

            return total_lp_wagered

    async def cancel_match(
        self, match, *, admin_cancelled=False, reported=False, reason="match cancelled"
    ):
        async with self.cancel_game_lock:
            match_settings = MatchSettings(
                type="mm",
                mode=match.get("mode", "base"),
                map="base",
                nrof_players=len(match["players"]),
            )

            lp_subtracted = False
            if match["approved"]:
                if len(match["players"]) == 2:
                    wager = min(
                        self.settings.get("max_wager_2p", float("inf")), match["wager"]
                    )
                else:
                    wager = match["wager"]

                n = len(match["players"])
                total_lp_wagered = (wager * n) + 5 * n * (n + 1)

                await self.add_lp_to_player(
                    match["winner"],
                    match_settings=match_settings,
                    match_results=MatchResults(
                        lp=-total_lp_wagered + wager,
                        matches=-1,
                        reason=reason,
                        normalized_wins=-len(match["players"]),
                    ),
                )
                lp_subtracted = True
                for player in match["players"]:
                    if player != match["winner"]:
                        await self.add_lp_to_player(
                            player,
                            match_settings=match_settings,
                            match_results=MatchResults(
                                lp=wager, matches=-1, reason=reason, normalized_wins=0
                            ),
                        )
            await self.db["matches"].update_one(
                {"_id": match["_id"]},
                {
                    "$set": {
                        "admin_cancelled": admin_cancelled,
                        "reported": reported,
                        "approved": False,
                        "admin_approved": False,
                    }
                },
            )
            return lp_subtracted

    @staticmethod
    async def get_random_quote():
        async with aiofiles.open("../data/quotes.txt") as f:
            lines = [i.strip() async for i in f if i.strip()]
            return random.choice(lines)

    @staticmethod
    def get_lp_filter_for_user(lp_filter, user=None):
        if user:
            lp_filter = {**lp_filter, "user": user["_id"]}
        return lp_filter

    async def get_player_by_id(self, player_id, projection=None):
        if projection is None:
            projection = {}
        return await self.db["users"].find_one(
            {"discord_username": player_id}, projection
        )

    def user_mentions_role_too_much(self, role, author):
        if author not in self.role_mention_times:
            self.role_mention_times[author] = {}

        time = datetime.datetime.now()
        last_time = self.role_mention_times[author].get(
            role, datetime.datetime(2000, 1, 1)
        )
        self.role_mention_times[author][role] = time
        return (time - last_time).total_seconds() < 3 * 60


if __name__ == "__main__":
    print(BotState.extract_nick('MrBurg#2444 "BugMuch"'))
