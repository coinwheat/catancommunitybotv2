import urllib
import aiofiles
import json
import motor
from motor import motor_asyncio


class DBConnection:
    def __init__(self):
        with open("secret.json", "r") as f:
            config = json.load(f)

        connection_string = f'mongodb+srv://{urllib.parse.quote_plus(config["db"]["user"])}:\
{urllib.parse.quote_plus(config["db"]["password"])}@{urllib.parse.quote_plus(config["db"]["host"])}/\
{urllib.parse.quote_plus(config["db"]["database"])}?retryWrites=true&w=majority'
        cluster = motor.motor_asyncio.AsyncIOMotorClient(connection_string)

        if not config["db"].get("database"):
            self.db = cluster.get_default_database()
        else:
            self.db = cluster[config["db"]["database"]]
