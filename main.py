import discord
from discord.ext import commands
from colorama import Back, Fore, Style
import time
import json
import platform
import os
import traceback
import sys
from utils import connection, botstate
import urllib
import motor
from motor import motor_asyncio

import functions
from functions import dbconnection


class Client(commands.Bot):
    def __init__(self):
        bot_intents = discord.Intents.default()
        bot_intents.typing = False
        bot_intents.presences = False
        bot_intents.members = True
        bot_intents.invites = True
        bot_intents.messages = True
        bot_intents.dm_messages = True
        bot_intents.guild_messages = True
        bot_intents.guild_reactions = True
        bot_intents.guild_scheduled_events = True
        bot_intents.guild_typing = True
        bot_intents.message_content = True
        super().__init__(
            command_prefix=commands.when_mentioned_or("."), intents=bot_intents
        )

    async def setup_hook(self):
        for file in os.listdir("./cogs"):
            if file.endswith(".py"):
                await self.load_extension(f"cogs.{file[:-3]}")


client = Client()


@client.event
async def on_ready():
    prfx = (
        Back.BLACK
        + Fore.GREEN
        + time.strftime("%H:%M:%S UTC", time.gmtime())
        + Back.RESET
        + Fore.WHITE
        + Style.BRIGHT
    )

    print(prfx + " Logged in as " + Fore.YELLOW + client.user.name)
    print(prfx + " Bot ID " + Fore.YELLOW + str(client.user.id))
    print(prfx + " Discord Version " + Fore.YELLOW + discord.__version__)
    print(prfx + " Python Version " + Fore.YELLOW + str(platform.python_version()))
    synced = await client.tree.sync()
    print(prfx + " Slash CMDs Synced " + Fore.YELLOW + str(len(synced)) + " Commands")
    games = await connection.db["matches"].count_documents({})
    players = await connection.db["users"].count_documents({})

    await client.change_presence(
        activity=discord.Game(f"{games:,} games and {players:,} players")
    )

    await botstate.init(client)


@client.event
async def on_error(method, *args, **kwargs):
    if len(args) >= 1 and isinstance(args[0], discord.Message):
        user = args[0].author
        link = args[0].jump_url
    elif len(args) >= 2 and isinstance(args[1], discord.Member):
        user = args[1]
        link = ""
    else:
        raise
    channel = discord.utils.get(
        user.guild.channels, id=botstate.settings.get("report_channel")
    )
    full_error = traceback.format_exc()
    context = repr(args)
    print(full_error, file=sys.stderr)
    await channel.send(
        (
            f"There was an error in the function `{method}`:\n"
            "```\n"
            f"{full_error}\n"
            "```"
            f"Context: {link}\n"
            f"```"
            f"{context}"
            "```"
        )[
            :2000
        ]  # Limit to max message size limit
    )


with open("secret.json", "r") as f:
    config = json.load(f)
    TOKEN = config["token"]


client.run(TOKEN)
