from collections import namedtuple

NO_RECENT_MATCH = "NO_RECENT_MATCH"
MALE_ROLE_ID = 894372075949654064
FEMALE_ROLE_ID = 894372075949654063

Setting = namedtuple("Setting", ("regex", "mapping", "message", "default"))

SETTINGS = {
    "min_wager": Setting(r"^\d+$", int, "min_wager must be a number", 25),
    "max_wager": Setting(r"^\d+$", int, "max_wager must be a number", 100),
    "max_wager_2p": Setting(
        r"^\d+$", int, "Max wager for 2 players must be a number", 999
    ),
    "wager_step": Setting(r"^\d+$", int, "wager_step must be a number", 5),
    "link": Setting(r"^https?://.*|^$", str, "link must be a link", ""),
    "report_channel": Setting(
        r"\d{18}", int, "Must be a valid channel number", 816413824163905546
    ),
    "bot_spam_channel": Setting(
        r"\d{18}", int, "Must be a valid channel number", 816413824163905546
    ),
    "first_game_bonus": Setting(r"^\d+$", int, "Must be a valid whole number", 0),
    "first_win_bonus": Setting(
        r"^\d+$", int, "First game win bonus must be a while number", 0
    ),
    "donator_role": Setting(r"^\d+$", int, "Donator must be a role id", 0),
    # Ranking Settings
    "rank_bracket_multiplier": Setting(
        r"^\d+(\.\d+)?", int, "Multiplier must be a decimal number", 1.5
    ),
    "rank_bonus_win_semifinal": Setting(
        r"^\d+", int, "Rank bonus win semifinal must be a whole number", 0
    ),
    "rank_bonus_win_final": Setting(
        r"^\d+", int, "Rank bonus win final must be a while number", 100
    ),
    "rank_bonus_sf_loser_bonus": Setting(
        r"^\d+", int, "Rank SF bonus must be a whole number", 0
    ),
    "qualifier_template": Setting(
        r"^https?://.^*$",
        int,
        "The qualifier template must be a link",
        "https://docs.google.com/spreadsheets/d/1GqYCtXU7QmWm8RNYq4GcFl5Sjs_4apmElagpxSf8BiY/edit",
    ),
    "qualifier_games_per_row": Setting(
        r"^\d+$", int, "Number of games each row in the qualifiers", 4
    ),
    "qualifier_horizontal_distance": Setting(
        r"^\d+$", int, "Horizontal spacing must be a number", 5
    ),
    "qualifier_vertical_distance": Setting(
        r"^\d+$", int, "Vertical spacing nust be a number", 3
    ),
    "qualifier_horizontal_offset": Setting(
        r"^\d+$", int, "Horizontal offset must be a number", 1
    ),
    "qualifier_vertical_offset": Setting(
        r"^\d+$", int, "Qualifier vertical spacing must be a number", 1
    ),
}
game_mode_emoji_mapping = {
    "seafarers": "⛵",
    "c&k": "⚔️",
    "base": "🎲",
    "c&k+seafarers": "🐙",
}

match_info_template = """
A {mode} match has been created with id `{id}` {link}
When you actually start the game, please type `!start {id}`
Current players are:

{players}
React with 🎮 to wager {wager} LP and join the game.
"""


game_mode_emoji_mapping = {
    "seafarers": "⛵",
    "c&k": "⚔️",
    "base": "🎲",
    "c&k+seafarers": "🐙",
}

game_mode_synonyms = {
    "sf": "seafarers",
    "ck": "c&k",
    "cities&knights": "c&k",
    "cities&knights+seafarers": "c&k+seafarers",
    "c&k+sf": "c&k+seafarers",
    "c&k&sf": "c&k+seafarers",
    "cksf": "c&k+seafarers",
    **{i: i for i in game_mode_emoji_mapping},
}

game_creation_synonyms = {
    "!base": "base",
    "!sf": "seafarers",
    "!ck": "c&k",
    "!cksf": "c&k+seafarers",
}

months = [
    "january",
    "february",
    "march",
    "april",
    "may",
    "june",
    "july",
    "august",
    "september",
    "october",
    "november",
    "december",
]

periods = ["month", "season", "year", "all"]
match_types = ["mm", "tourney"]

match_type_synonyms = {
    "matchmaker": "mm",
    "tournaments": "tourney",
    "tournament": "tourney",
    **{i: i for i in match_types},
}
