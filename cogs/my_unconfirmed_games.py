import traceback

import discord
from discord.ext import commands
from discord import app_commands
import functions
from utils import connection, botstate


class my_unconfirmed_games(commands.Cog):
    def __init__(self, client: commands.Bot):
        self.client = client

    @app_commands.command(
        name="my_unconfirmed_games",
        description="Games I played but are yet not confirmed",
    )
    async def my_unconfirmed_games(self, interaction: discord.Interaction):
        user = interaction.user

        game_check = await connection.db["matches"].find_one(
            {
                "winner": {"$type": 2},
                "players": botstate.normalize_mention(user.mention),
                "approved": False,
                "reported": {"$exists": False},
            }
        )
        games = connection.db["matches"].find(
            {
                "winner": {"$type": 2},
                "players": botstate.normalize_mention(user.mention),
                "approved": False,
                "reported": {"$exists": False},
            }
        )
        msg_to_send = ""
        if game_check is not None:
            response_message = "Games you have played in that need confirmation:\n"
        else:
            response_message = (
                "Thanks from the CC Team. You have no games that need confirmation"
            )
        msg_to_send += response_message
        async for game in games:
            try:
                match_winner = await botstate.get_player_by_id(
                    game["winner"], {"display_name": 1}
                )
                winner_name = match_winner["display_name"]
                msg_to_send += (
                    f"game `{game['id']}` by `{winner_name}`\n"
                    f"{game.get('winner_message_url', 'no link available')}"
                )

            except Exception:
                traceback.print_exc()
                await interaction.response.send_message(
                    f"Error getting data for game `{game['id']}`"
                )

        await interaction.response.send_message(msg_to_send)


async def setup(client: commands.Bot) -> None:
    await client.add_cog(my_unconfirmed_games(client))
