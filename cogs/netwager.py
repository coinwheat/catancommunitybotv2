import discord
from discord.ext import commands
from discord import app_commands
import functions
from utils import connection, botstate


class netwager(commands.Cog):
    def __init__(self, client: commands.Bot):
        self.client = client

    @app_commands.command(name="netwager", description="Netwager!")
    @app_commands.describe(discord_user="Mention the user")
    async def netwager(
        self, interaction: discord.Interaction, discord_user: discord.Member = None
    ):
        if discord_user is None:
            user = interaction.user
        else:
            user = discord_user

        wager_matches = []
        wager_matches = (
            await connection.db["matches"]
            .find(
                {
                    "$and": [
                        {
                            "players": {
                                "$in": [botstate.normalize_mention(user.mention)]
                            }
                        },
                        {"wager": {"$ne": 0}},
                        {"winner": {"$ne": None}},
                    ]
                },
                {"wager": 1, "winner": 1, "players": 1},
            )
            .to_list(99999)
        )

        if len(wager_matches) == 0:
            await interaction.response.send_message(
                "This player has not wagered any league points", ephemeral=True
            )
            return

        net_profit_or_loss = 0
        total_wagered_league_points = 0

        for match in wager_matches:
            total_wagered_league_points += match["wager"]
            if match["winner"] == user.mention:
                net_profit_or_loss += match["wager"] * (len(match["players"]) - 1)
            else:
                net_profit_or_loss -= match["wager"]

        if net_profit_or_loss > 3000:
            profit_or_loss_statement = (
                f"and has an INSANE net profit of {net_profit_or_loss}! "
                f"Congratulations - but please remember sharing is caring"
            )
        elif net_profit_or_loss > 0:
            profit_or_loss_statement = (
                f"and has a net profit of {net_profit_or_loss} ! Hooray, keep it up!"
            )
        elif 0 > net_profit_or_loss >= -400:
            profit_or_loss_statement = (
                f"and has a net loss of {net_profit_or_loss} ! Uh-oh, don't worry "
                f"there's always more points to be won!"
            )
        elif net_profit_or_loss < -400:
            profit_or_loss_statement = f"and has a net loss of {net_profit_or_loss} ! Maybe the player should stick to non-wager games."
        else:
            profit_or_loss_statement = (
                "and has broken even! I'm not sure whether you should be happy or sad."
            )

        await interaction.response.send_message(
            f"{user.name} has wagered a total of {total_wagered_league_points} LPs over {len(wager_matches)} games"
            f" {profit_or_loss_statement}",
            ephemeral=True,
        )


async def setup(client: commands.Bot) -> None:
    await client.add_cog(netwager(client))
