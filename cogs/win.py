import discord
from discord.ext import commands
from discord import app_commands
import functions
from utils import connection, botstate

from misc_files.constants import (
    NO_RECENT_MATCH,
    game_creation_synonyms,
    game_mode_emoji_mapping,
    game_mode_synonyms,
)


class win(commands.Cog):
    def __init__(self, client: commands.Bot):
        self.client = client

    @app_commands.command(
        name="win", description="Use this command to specify game winner"
    )
    @app_commands.describe(discord_user="Mention the user")
    async def win(
        self,
        interaction: discord.Interaction,
        discord_user: discord.Member = None,
        game_id: str = None,
    ):
        if discord_user is None:
            user = interaction.user
        else:
            user = discord_user

        if isinstance(interaction.channel, discord.DMChannel):
            await interaction.response.send_message(
                "You cannot use this command via direct message"
            )
            return

        # words = botstate.split_words(interaction.content)
        if game_id is None:
            game_id = (
                await botstate.get_player_by_id(
                    botstate.normalize_mention(user.mention),
                    {"last_game_id": True},
                )
            ).get("last_game_id")

        if game_id is None or game_id == NO_RECENT_MATCH:
            await interaction.response.send_message(
                "Can not determine the last game you participated in."
            )
            return

        message_to_send = ""
        message_to_send += f"The last game you participated in was `{game_id}`.\n"

        if not botstate.is_valid_game_id(game_id):
            await interaction.response.send_message(
                "Please double check if you typed the game id correctly."
            )
            return
        attachments = interaction.op.attachments()
        if attachments is None:
            message_to_send += "Please remember to also post a screenshot.\n"
            # No return here, message is valid

        match = await botstate.get_game_by_id(game_id)
        if not match:
            await interaction.response.send_message(
                "Double check your game id: `" + game_id + "`"
            )
            return
        if match["winner"] is not None:
            await interaction.response.send_message(
                f"This match is already pending a win by {match['winner']}, if this was an error"
                f" please contact a moderator"
            )
            return

        winner = botstate.normalize_mention(user.mention)
        if winner in ["@everyone", "@here"] or user is None:
            await interaction.response.send_message("Nice try!!!!")
            return
        elif botstate.normalize_mention(user.mention) not in match["players"]:
            await interaction.response.send_message(
                f"You can only submit the results for a match you are a part of.\n"
                + winner
                + f" did not play in match "
                + repr(game_id)
                + f", make sure the match id and player id are properly spelled, and that "
                + winner
                + f" properly reacted to the original message"
            )
            return

        if len(match["players"]) == 1:
            await interaction.response.send_message(
                " You cannot play a match with only one player."
            )
            return
        if match.get("admin_cancelled", False):
            await interaction.response.send_message(
                "This match has been cancelled by a mod"
            )
            return

        if (
            match.get("wager", 0) > botstate.settings.get("max_wager_2p", float("inf"))
            and len(match["players"]) <= 2
        ):
            message_to_send += "Note: Wager has been capped at " + str(
                botstate.settings.get("max_wager_2p", 0)
            )

        message_to_send += ", ".join(match["players"])
        +", please react with :thumbsup: to confirm the result of match "
        +functions.game_mode_emoji_mapping[match.get("mode", "base")]
        +" `"
        +game_id
        +"`"
        +"\nTo flag this match for moderator attention, please react with 🛡️"

        await connection.db["matches"].update_one(
            {"_id": match["_id"]},
            {
                "$push": {"approved_players": user},
                "$set": {
                    "winner": winner,
                    "winner_display_name": botstate.get_user_display_name(user),
                    "winner_message_url": interaction.jump_url,
                },
            },
        )
        interaction.response.send_message(message_to_send)
        await interaction.add_reaction("👍")
        await interaction.add_reaction("🛡️")

        botstate.message_ids[message_to_send.id] = match["_id"]


async def setup(client: commands.Bot) -> None:
    await client.add_cog(win(client))
